package com.paragon.enums;

public enum TileRotation {
    ZERO(0),
    ONE(60),
    TWO(120),
    THREE(180),
    FOUR(240),
    FIVE(300);

    private final int rotation;

    private TileRotation(int rotation) {
        this.rotation = rotation;
    }

    public static TileRotation getInstance(int rotation) {
        switch (rotation) {
            case 0:
                return ZERO;
            case 60:
                return ONE;
            case 120:
                return TWO;
            case 180:
                return THREE;
            case 240:
                return FOUR;
            case 300:
                return FIVE;
        }
        return null;
    }

    public int getRotation() {
        return rotation;
    }

    public TileRotation getOpposite() {
        switch (this) {
            case ZERO:
                return THREE;
            case ONE:
                return FOUR;
            case TWO:
                return FIVE;
            case THREE:
                return ZERO;
            case FOUR:
                return ONE;
            case FIVE:
                return TWO;
        }
        return null;
    }

}
