package com.paragon.enums;

public enum GameStatus {
    INITIALIZED,
    RUNNING,
    FINISHED;
    
    private GameStatus() {
        
    }
    
}
