package com.paragon.enums;

import java.awt.Color;

public enum TColor {
    BLUE("blue"),
    GREEN("green"),
    RED("red"),
    YELLOW("yellow");

    private final static Color TRED = new Color(220, 33, 33);
    private final static Color TGREEN = new Color(41, 144, 10);
    private final static Color TBLUE = new Color(24, 157, 235);
    private final static Color TYELLOW = new Color(239, 239, 77);

    private String name;

    private TColor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Color getColor() {

        switch (this) {
            case BLUE:
                return TBLUE;
            case GREEN:
                return TGREEN;
            case RED:
                return TRED;
            case YELLOW:
                return TYELLOW;
            default:
                return null;
        }
    }

}
