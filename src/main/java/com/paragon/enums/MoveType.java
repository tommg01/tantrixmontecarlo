package com.paragon.enums;

public enum MoveType {
    FREE(),
    FORCED_BEFORE_FREE(),
    FORCED_AFTER_FREE();
    
    private MoveType() {
    }
    
}
