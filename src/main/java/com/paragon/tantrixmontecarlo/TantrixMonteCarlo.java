package com.paragon.tantrixmontecarlo;

import com.paragon.enums.TColor;
import com.paragon.objects.Game;
import com.paragon.objects.Player;
import com.paragon.objects.Tile;
import com.paragon.objects.agents.Agent;
import com.paragon.objects.agents.LineBuilder1Agent;
import com.paragon.objects.agents.MonteCarlo1Agent;
import com.paragon.objects.agents.RandomAgent;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

public class TantrixMonteCarlo {

    private static Set<Game> games = Collections.synchronizedSet(new HashSet<>());
    private static int gameCount = 0;
    private static double totalTPs = 0.0;
    private static Map<Integer, Integer> scoreDifferences = new HashMap<>();
    private static ReentrantLock gameAddLock = new ReentrantLock();

    public static void main(String... args) {

//        Set<TColor[]> sequences = new LinkedHashSet<>();
//        sequences.add(new TColor[]{TColor.BLUE, TColor.BLUE, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.GREEN, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.BLUE, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.GREEN, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.BLUE, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.GREEN, TColor.GREEN});
//
//        sequences.add(new TColor[]{TColor.BLUE, TColor.BLUE, TColor.RED});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.RED, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.RED, TColor.BLUE, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.RED, TColor.RED, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.RED, TColor.BLUE, TColor.RED});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.RED, TColor.RED});
//
//        sequences.add(new TColor[]{TColor.BLUE, TColor.BLUE, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.YELLOW, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.BLUE, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.YELLOW, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.BLUE, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.YELLOW, TColor.YELLOW});
//
//        sequences.add(new TColor[]{TColor.RED, TColor.RED, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.RED, TColor.GREEN, TColor.RED});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.RED, TColor.RED});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.GREEN, TColor.RED});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.RED, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.RED, TColor.GREEN, TColor.GREEN});
//
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.YELLOW, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.GREEN, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.YELLOW, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.GREEN, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.YELLOW, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.GREEN, TColor.GREEN});
//
//        sequences.add(new TColor[]{TColor.RED, TColor.RED, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.RED, TColor.YELLOW, TColor.RED});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.RED, TColor.RED});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.YELLOW, TColor.RED});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.RED, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.RED, TColor.YELLOW, TColor.YELLOW});
//
//        sequences.add(new TColor[]{TColor.BLUE, TColor.GREEN, TColor.RED});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.RED, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.BLUE, TColor.RED});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.RED, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.RED, TColor.BLUE, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.RED, TColor.GREEN, TColor.BLUE});
//
//        sequences.add(new TColor[]{TColor.BLUE, TColor.GREEN, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.YELLOW, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.BLUE, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.YELLOW, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.BLUE, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.GREEN, TColor.BLUE});
//
//        sequences.add(new TColor[]{TColor.BLUE, TColor.YELLOW, TColor.RED});
//        sequences.add(new TColor[]{TColor.BLUE, TColor.RED, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.BLUE, TColor.RED});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.RED, TColor.BLUE});
//        sequences.add(new TColor[]{TColor.RED, TColor.BLUE, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.RED, TColor.YELLOW, TColor.BLUE});
//
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.GREEN, TColor.RED});
//        sequences.add(new TColor[]{TColor.YELLOW, TColor.RED, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.YELLOW, TColor.RED});
//        sequences.add(new TColor[]{TColor.GREEN, TColor.RED, TColor.YELLOW});
//        sequences.add(new TColor[]{TColor.RED, TColor.YELLOW, TColor.GREEN});
//        sequences.add(new TColor[]{TColor.RED, TColor.GREEN, TColor.YELLOW});
//
//        for (TColor[] sequence : sequences) {
//            int count = 0;
//            for (Tile tile : Tile.getAllTiles()) {
//                for (int i = 0; i < 6; i++) {
//                    if (tile.getSequence()[i].equals(sequence[0])
//                            && tile.getSequence()[(i + 1) % 6].equals(sequence[1])
//                            && tile.getSequence()[(i + 2) % 6].equals(sequence[2])) {
//                        count++;
//                        break;
//                    }
//                }
//            }
//            System.out.println(Arrays.toString(sequence) + "\t" + count);
//        }
        Player[] players = new Player[2];
        players[0] = new Player("P1", TColor.GREEN);
        players[1] = new Player("P2", TColor.BLUE);
        long startTime = System.currentTimeMillis();
        int numberOfGames = 5000;
        Agent[] agents = new Agent[2];
        for (int i = 0; i < numberOfGames; i++) {
            agents[0] = new RandomAgent(players[0].getName(), true, i);
            agents[1] = new MonteCarlo1Agent(players[1].getName(), false, i);
            Game game = new Game(agents, players, 5, false);
            game.run();
//            Thread gameThread = new Thread(game);
//            gameThread.start();
        }
//        while (games.size() < numberOfGames) {
//            try {
//                Thread.sleep(10);
//            } catch (Exception e) {
//            }
//        }

        long totalTime = System.currentTimeMillis() - startTime;
        System.out.println("Speed: " + String.format("%.0f", 1.0 * numberOfGames / totalTime * 1000) + " g/s");
        System.out.println("Games played: " + gameCount);
        System.out.println("Total TPs for player 1: " + totalTPs);
        for (Entry<Integer, Integer> entry : scoreDifferences.entrySet()) {
            System.out.println(entry.getKey() + "\t" + entry.getValue());
        }
    }

    public static void addFinishedGame(Game game) {
        if (game.getCurrentGameState().isGameOver()) {
            while (!gameAddLock.tryLock()) {
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                }
            }
            gameCount++;
            int[] scores = game.getCurrentGameState().getScores();
            double tps = game.getCurrentGameState().getTPScoreFromScores(scores);
            totalTPs += tps;
            System.out.printf("%.1f\t" + Arrays.toString(scores) + "\n", tps);

            int scoreDiff = scores[0] - scores[1];
            if (!scoreDifferences.containsKey(scoreDiff)) {
                scoreDifferences.put(scoreDiff, 0);
            }
            scoreDifferences.put(scoreDiff, scoreDifferences.get(scoreDiff) + 1);
        }
    }

}
