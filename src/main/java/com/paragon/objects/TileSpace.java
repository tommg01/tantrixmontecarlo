package com.paragon.objects;

import com.paragon.enums.TColor;
import com.paragon.enums.TileRotation;
import java.util.HashMap;
import java.util.Map;

public abstract class TileSpace {
    
    protected int[] coordinates;
    protected TColor[] sequence = {null, null, null, null, null, null};
    
    public int[] getCoordinates() {
        return coordinates;
    }
    
    public TColor[] getSequence() {
        return sequence;
    }
    
    public int getIntegratedCoordinates() {
        return integrateCoordinates(coordinates);
    }
    
    public static int integrateCoordinates(int[] coordinates) {
        return coordinates[0] * 100 + coordinates[1];
    }
    
    public static int[] splitCoordinates(int i) {
        int mod = (i + 100000) % 100;
        if (mod > 50) {
            mod -= 100;
        }
        return new int[] {(i - mod) / 100, mod};
    }
    
    public Map<Integer, TColor[]> getNeighboringTileSpaces() {
        Map<Integer, TColor[]> neighboringTileSpaces = new HashMap<>();
        int[] neighbor = new int[2];
        TColor[] colors = new TColor[] {null, null, null, sequence[0], null, null};
        neighbor[0] = coordinates[0] - 1;
        neighbor[1] = coordinates[1];
        neighboringTileSpaces.put(integrateCoordinates(neighbor), colors);
        colors = new TColor[] {null, null, null, null, sequence[1], null};
        neighbor[0] = coordinates[0];
        neighbor[1] = coordinates[1] - 1;
        neighboringTileSpaces.put(integrateCoordinates(neighbor), colors);
        colors = new TColor[] {null, null, null, null, null, sequence[2]};
        neighbor[0] = coordinates[0] + 1;
        neighbor[1] = coordinates[1] - 1;
        neighboringTileSpaces.put(integrateCoordinates(neighbor), colors);
        colors = new TColor[] {sequence[3], null, null, null, null, null};
        neighbor[0] = coordinates[0] + 1;
        neighbor[1] = coordinates[1];
        neighboringTileSpaces.put(integrateCoordinates(neighbor), colors);
        colors = new TColor[] {null, sequence[4], null, null, null, null};
        neighbor[0] = coordinates[0];
        neighbor[1] = coordinates[1] + 1;
        neighboringTileSpaces.put(integrateCoordinates(neighbor), colors);
        colors = new TColor[] {null, null, sequence[5], null, null, null};
        neighbor[0] = coordinates[0] - 1;
        neighbor[1] = coordinates[1] + 1;
        neighboringTileSpaces.put(integrateCoordinates(neighbor), colors);
        return neighboringTileSpaces;
    }
    
    public Map<TileRotation, Integer> getNeighboringTileSpacesWithRotation() {
        Map<TileRotation, Integer> neighboringTileSpaces = new HashMap<>();
        int[] neighbor = new int[2];
        neighbor[0] = coordinates[0] - 1;
        neighbor[1] = coordinates[1];
        neighboringTileSpaces.put(TileRotation.ZERO, integrateCoordinates(neighbor));
        neighbor[0] = coordinates[0];
        neighbor[1] = coordinates[1] - 1;
        neighboringTileSpaces.put(TileRotation.ONE, integrateCoordinates(neighbor));
        neighbor[0] = coordinates[0] + 1;
        neighbor[1] = coordinates[1] - 1;
        neighboringTileSpaces.put(TileRotation.TWO, integrateCoordinates(neighbor));
        neighbor[0] = coordinates[0] + 1;
        neighbor[1] = coordinates[1];
        neighboringTileSpaces.put(TileRotation.THREE, integrateCoordinates(neighbor));
        neighbor[0] = coordinates[0];
        neighbor[1] = coordinates[1] + 1;
        neighboringTileSpaces.put(TileRotation.FOUR, integrateCoordinates(neighbor));
        neighbor[0] = coordinates[0] - 1;
        neighbor[1] = coordinates[1] + 1;
        neighboringTileSpaces.put(TileRotation.FIVE, integrateCoordinates(neighbor));
        return neighboringTileSpaces;
    }
    
    public int getNeighboringTileSpace(TileRotation rotation) {
        Map<TileRotation, Integer> neighbors =  getNeighboringTileSpacesWithRotation();
        return neighbors.get(rotation);
    }
    
}
