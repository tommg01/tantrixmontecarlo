package com.paragon.objects.frames;

import com.paragon.objects.FreeTileSpace;
import com.paragon.objects.GameState;
import com.paragon.objects.PlacedTile;
import com.paragon.objects.Player;
import com.paragon.objects.Tile;
import com.paragon.objects.TileSpace;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static java.util.stream.Collectors.toSet;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Board extends JFrame {

    private static final int PIXEL_WIDTH = 73;
    private static final int PIXEL_HEIGHT = 84;
    private static final int SCREEN_WIDTH;
    private static final int SCREEN_HEIGHT;
    private final static int TABLE_SIZE = 55;
    private int[][] tableCoordinates;
    private Map<Integer, int[]> coordinatesReverseMap;
    private TileSpace[][] tileSpaces;
    private static BufferedImage emptySpaceImage;
    private static BufferedImage forcedSpaceImage;
    private static BufferedImage forbiddenSpaceImage;
    private static BufferedImage[][] tileImagesWithRotation;

    private GameState gameState;
    private final Player[] players;

    static {
        try {
            emptySpaceImage = ImageIO.read(new File("tiles/tile0.png"));
            forcedSpaceImage = ImageIO.read(new File("tiles/forced.png"));
            forbiddenSpaceImage = ImageIO.read(new File("tiles/forbidden.png"));
            tileImagesWithRotation = new BufferedImage[56][6];
            for (int i = 1; i <= 56; i++) {
                for (int j = 0; j < 6; j++) {
                    tileImagesWithRotation[i - 1][j] = ImageIO.read(new File("tiles/tile" + i + "_" + j + ".png"));
                }
            }
        } catch (IOException e) {
            System.out.println("Couldn't load image file.");
        }

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        SCREEN_WIDTH = (int) screenSize.getWidth();
        SCREEN_HEIGHT = (int) screenSize.getHeight();
    }

    public Board(GameState gameState, Player[] players) {
        super();
        this.gameState = gameState;
        this.players = players;
    }

    public void display() {
        setLayout(new BorderLayout());
        setTitle("Tantrix board");
        setLocationRelativeTo(null);
        setLocation(new Point(0, 0));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        setResizable(true);

        JPanel panel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);

                setTableCoordinates();

                setTileSpaces();

                Map<Integer, FreeTileSpace> legitimateSpaces = new HashMap<>();

                // Get all adjoining spaces
                legitimateSpaces.putAll(gameState.getBoardState().getAllAdjoiningSpaces());

                // Get all forced spaces
                Set<FreeTileSpace> forcedSpaces = legitimateSpaces.values().stream().filter(l -> l.isForced()).collect(toSet());

                // Get all forbidden spaces
                Set<Integer> forbiddenSpaces = gameState.getBoardState().findForbiddenSpaces(forcedSpaces, legitimateSpaces);

                Set<Integer> forcedSpacesCoordinates = new HashSet<>();
                for (FreeTileSpace forcedSpace : forcedSpaces) {
                    forcedSpacesCoordinates.add(forcedSpace.getIntegratedCoordinates());
                }

                for (int i = 0; i < TABLE_SIZE; i++) {
                    for (int j = 0; j < TABLE_SIZE; j++) {
                        int[] splitCoordinates = TileSpace.splitCoordinates(tableCoordinates[i][j]);
                        g.setFont(new Font("Arial", Font.PLAIN, 9));
                        if (tileSpaces[i][j] == null) {
                            if (forcedSpacesCoordinates.contains(tableCoordinates[i][j])) {
                                g.drawImage(forcedSpaceImage, PIXEL_WIDTH * j + PIXEL_WIDTH * (i % 2) / 2, PIXEL_HEIGHT * i * 3 / 4, null);
                            } else if (forbiddenSpaces.contains(tableCoordinates[i][j])) {
                                g.drawImage(forbiddenSpaceImage, PIXEL_WIDTH * j + PIXEL_WIDTH * (i % 2) / 2, PIXEL_HEIGHT * i * 3 / 4, null);
                            } else {
                                g.drawImage(emptySpaceImage, PIXEL_WIDTH * j + PIXEL_WIDTH * (i % 2) / 2, PIXEL_HEIGHT * i * 3 / 4, null);
                            }
                            g.drawString("(" + splitCoordinates[0] + ", " + splitCoordinates[1] + ")", PIXEL_WIDTH * j + PIXEL_WIDTH * (i % 2) / 2 + PIXEL_WIDTH / 2, PIXEL_HEIGHT * i * 3 / 4 + PIXEL_HEIGHT * 3 / 8);
                        } else {
                            int tileId = ((PlacedTile) tileSpaces[i][j]).getTile().getId();
                            int rotation = ((PlacedTile) tileSpaces[i][j]).getRotation().getRotation() / 60;
                            g.drawImage(tileImagesWithRotation[tileId - 1][rotation], PIXEL_WIDTH * j + PIXEL_WIDTH * (i % 2) / 2, PIXEL_HEIGHT * i * 3 / 4, null);
                        }
                    }
                }

            }

        };

        panel.setPreferredSize(new Dimension(PIXEL_WIDTH * TABLE_SIZE + PIXEL_WIDTH / 2, PIXEL_HEIGHT + PIXEL_HEIGHT * (TABLE_SIZE - 1) * 3 / 4));

        JPanel p1TilesPanel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);

                g.setColor(players[0].getColor().getColor());
                g.fillRect(0, 0, PIXEL_WIDTH + 20, 50);
                g.setColor(Color.black);
                g.setFont(new Font("Arial", Font.BOLD, 15));
                g.drawString(players[0].getName(), 20, 20);
                if (gameState.isPlayer1ToMove()) {
                    g.setColor(Color.cyan);
                } else {
                    g.setColor(Color.white);
                }
                g.fillRect(0, 50, PIXEL_WIDTH + 20, SCREEN_HEIGHT);

                int tileNumber = 0;
                for (Tile tile : gameState.getPlayer1Tiles()) {
                    g.drawImage(tileImagesWithRotation[tile.getId() - 1][0], 10, 60 + (PIXEL_HEIGHT + 10) * tileNumber, null);
                    g.setFont(new Font("Arial", Font.PLAIN, 9));
                    g.setColor(Color.black);
                    g.drawString(String.valueOf(tile.getId()), 5 + PIXEL_WIDTH, 60 + (PIXEL_HEIGHT + 10) * tileNumber);
                    tileNumber++;
                }

            }

        };
        p1TilesPanel.setPreferredSize(new Dimension(PIXEL_WIDTH + 20, SCREEN_HEIGHT));

        add(p1TilesPanel, BorderLayout.LINE_START);

        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
        scrollPane.getViewport().setViewPosition(new Point(PIXEL_WIDTH * (TABLE_SIZE - 1) / 2 - (SCREEN_WIDTH - 2 * (PIXEL_WIDTH + 20)) / 2, PIXEL_HEIGHT * (TABLE_SIZE - 1) * 3 / 8 - (SCREEN_HEIGHT / 2)));
        add(scrollPane, BorderLayout.CENTER);

        JPanel p2TilesPanel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);

                g.setColor(players[1].getColor().getColor());
                g.fillRect(0, 0, PIXEL_WIDTH + 20, 50);
                g.setColor(Color.black);
                g.setFont(new Font("Arial", Font.BOLD, 15));
                g.drawString(players[1].getName(), 20, 20);
                if (!gameState.isPlayer1ToMove()) {
                    g.setColor(Color.cyan);
                } else {
                    g.setColor(Color.white);
                }
                g.fillRect(0, 50, PIXEL_WIDTH + 20, SCREEN_HEIGHT);

                int tileNumber = 0;
                for (Tile tile : gameState.getPlayer2Tiles()) {
                    g.drawImage(tileImagesWithRotation[tile.getId() - 1][0], 10, 60 + (PIXEL_HEIGHT + 10) * tileNumber, null);
                    g.setFont(new Font("Arial", Font.PLAIN, 9));
                    g.setColor(Color.black);
                    g.drawString(String.valueOf(tile.getId()), 5 + PIXEL_WIDTH, 60 + (PIXEL_HEIGHT + 10) * tileNumber);
                    tileNumber++;
                }

//                int[] scores = gameState.getScores();
//                System.out.println(players[0].getColor().getName() + ": " + scores[0]);
//                System.out.println(players[1].getColor().getName() + ": " + scores[1]);
            }

        };
        p2TilesPanel.setPreferredSize(new Dimension(PIXEL_WIDTH + 20, SCREEN_HEIGHT));

        add(p2TilesPanel, BorderLayout.LINE_END);

    }

    private void setTableCoordinates() {
        int middlePointX = TABLE_SIZE / 2;
        int middlePointY = TABLE_SIZE / 2;
        tableCoordinates = new int[TABLE_SIZE][TABLE_SIZE];
        coordinatesReverseMap = new HashMap<>();
        for (int i = 0; i < TABLE_SIZE; i++) {
            for (int j = 0; j < TABLE_SIZE; j++) {
                tableCoordinates[i][j] = -99999;
            }
        }
        tableCoordinates[middlePointX][middlePointY] = 0;
        coordinatesReverseMap.put(0, new int[]{middlePointX, middlePointY});
        setNeighboringCoordinates(middlePointX, middlePointY, 0);
    }

    private void setNeighboringCoordinates(int x, int y, int integratedCoordinates) {
        for (int i = 0; i < 6; i++) {
            int[] tableCoordinatesOffset = getTableCoordinatesOffsetForRotation(x, i);
            int integratedCoordinatesOffset = getIntegratedCoordinatesOffsetForRotation(i);
            int newX = x + tableCoordinatesOffset[0];
            int newY = y + tableCoordinatesOffset[1];
            if (newX >= 0
                    && newX < TABLE_SIZE
                    && newY >= 0
                    && newY < TABLE_SIZE) {
                if (tableCoordinates[newX][newY] == -99999) {
                    int newIntegratedCoordinates = integratedCoordinates + integratedCoordinatesOffset;
                    tableCoordinates[newX][newY] = newIntegratedCoordinates;
                    coordinatesReverseMap.put(newIntegratedCoordinates, new int[]{newX, newY});
                    setNeighboringCoordinates(newX, newY, newIntegratedCoordinates);
                }
            }
        }
    }

    private int getIntegratedCoordinatesOffsetForRotation(int rotation) {
        switch (rotation) {
            case 0:
                return -100;
            case 1:
                return -1;
            case 2:
                return 99;
            case 3:
                return 100;
            case 4:
                return 1;
            case 5:
                return -99;
            default:
                return -999999;
        }
    }

    private int[] getTableCoordinatesOffsetForRotation(int rowIndex, int rotation) {
        if (rowIndex % 2 == 0) {
            switch (rotation) {
                case 0:
                    return new int[]{0, -1};
                case 1:
                    return new int[]{-1, -1};
                case 2:
                    return new int[]{-1, 0};
                case 3:
                    return new int[]{0, 1};
                case 4:
                    return new int[]{1, 0};
                case 5:
                    return new int[]{1, -1};
                default:
                    return new int[]{};
            }
        } else {
            switch (rotation) {
                case 0:
                    return new int[]{0, -1};
                case 1:
                    return new int[]{-1, 0};
                case 2:
                    return new int[]{-1, 1};
                case 3:
                    return new int[]{0, 1};
                case 4:
                    return new int[]{1, 1};
                case 5:
                    return new int[]{1, 0};
                default:
                    return new int[]{};
            }
        }

    }

    private void setTileSpaces() {
        tileSpaces = new TileSpace[TABLE_SIZE][TABLE_SIZE];
        for (Entry<Integer, PlacedTile> placedTile : this.gameState.getBoardState().getPlacedTiles().entrySet()) {
            int[] tableCoordinatesForPlacedTile = coordinatesReverseMap.get(placedTile.getKey());
            tileSpaces[tableCoordinatesForPlacedTile[0]][tableCoordinatesForPlacedTile[1]] = placedTile.getValue();
        }
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
        repaint();
    }

}
