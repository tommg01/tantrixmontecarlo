package com.paragon.objects;

import com.paragon.enums.TileRotation;

public class PlacedTile extends TileSpace {

    private final Tile tile;
    private final TileRotation rotation;

    public PlacedTile(Tile tile, int[] coordinates, TileRotation rotation) {
        this.tile = tile;
        this.coordinates = coordinates;
        this.rotation = rotation;
        for (int i = 0; i < 6; i++) {
            sequence[(i + rotation.getRotation() / 60) % 6] = tile.getSequence()[i];
        }
    }

    public Tile getTile() {
        return tile;
    }

    public TileRotation getRotation() {
        return rotation;
    }

    @Override
    public String toString() {
        return String.valueOf(tile.getId());
    }

}
