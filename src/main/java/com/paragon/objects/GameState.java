package com.paragon.objects;

import com.paragon.enums.TColor;
import com.paragon.enums.MoveType;
import com.paragon.enums.TileRotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class GameState {

    private final BoardState boardState;
    private final BagOfTiles bagOfTiles;
    private final List<Tile> player1Tiles;
    private final List<Tile> player2Tiles;
    private final Player[] players;
    private boolean gameOver;
    private Set<Move> legalMoves;
    private boolean freeMoveHasBeenPlayed;
    private boolean player1ToMove;

    public GameState(Player[] players) {
        this.players = players;
        boardState = new BoardState();
        bagOfTiles = new BagOfTiles();
        bagOfTiles.fill();
        player1Tiles = new ArrayList<>();
        player2Tiles = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            player1Tiles.add(bagOfTiles.draw());
            player2Tiles.add(bagOfTiles.draw());
        }
        freeMoveHasBeenPlayed = false;
        player1ToMove = Math.random() > 0.5;

        legalMoves = findLegalMoves();
    }

    public GameState(BoardState boardState, List<Tile> player1Tiles, List<Tile> player2Tiles, boolean freeMoveHasBeenPlayed, boolean player1ToMove, Player[] players, BagOfTiles bagOfTiles) {
        this.boardState = boardState;
        this.player1Tiles = player1Tiles;
        this.player2Tiles = player2Tiles;
        this.freeMoveHasBeenPlayed = freeMoveHasBeenPlayed;
        this.player1ToMove = player1ToMove;
        this.bagOfTiles = bagOfTiles;
        this.players = players;

        legalMoves = findLegalMoves();
        if (legalMoves.isEmpty()) {
            switchPlayer();
            legalMoves = findLegalMoves();
            if (legalMoves.isEmpty()) {
                if (player1Tiles.isEmpty() && player2Tiles.isEmpty()) {
                    gameOver = true;
                } else {
                    switchPlayer();
                    legalMoves = findLegalMoves();
                    if (legalMoves.isEmpty()) {
                        gameOver = true;
                    }
                }
            }
        }
    }

    public GameState(BoardState boardState, List<Tile> player1Tiles, List<Tile> player2Tiles, boolean freeMoveHasBeenPlayed, boolean player1ToMove, Player[] players, BagOfTiles bagOfTiles, Set<Move> legalMoves) {
        this.boardState = boardState;
        this.player1Tiles = player1Tiles;
        this.player2Tiles = player2Tiles;
        this.freeMoveHasBeenPlayed = freeMoveHasBeenPlayed;
        this.player1ToMove = player1ToMove;
        this.bagOfTiles = bagOfTiles;
        this.players = players;
        this.legalMoves = legalMoves;
    }

    public BoardState getBoardState() {
        return boardState;
    }

    public List<Tile> getPlayer1Tiles() {
        return player1Tiles;
    }

    public List<Tile> getPlayer2Tiles() {
        return player2Tiles;
    }

    public BagOfTiles getBagOfTiles() {
        return bagOfTiles;
    }

    public GameState deepClone() {
        BoardState _boardState = this.boardState.deepClone();
        List<Tile> _player1Tiles = new ArrayList<>();
        _player1Tiles.addAll(this.player1Tiles);
        List<Tile> _player2Tiles = new ArrayList<>();
        _player2Tiles.addAll(this.player2Tiles);
        BagOfTiles _bagOfTiles = this.bagOfTiles.deepClone();
        return new GameState(_boardState, _player1Tiles, _player2Tiles, this.freeMoveHasBeenPlayed, this.player1ToMove, this.players, _bagOfTiles, legalMoves);
    }

    public GameState transition(Move move, boolean temporaryTransition, String source) {
        if (!legalMoves.contains(move)) {
            System.out.println("WTF " + source);
            System.exit(1);
        }
//        System.out.println(boardState.getPlacedTiles().size() + "\t" + move.getPlayer().getName() + "\t" + move.getMoveType() + "\t" + Arrays.toString(move.getPlacedTile().getCoordinates()) + "\t" + Arrays.toString(move.getPlacedTile().getSequence()));
        BoardState newBoardState = boardState.transition(move);
        List<Tile> newPlayer1Tiles = new ArrayList<>();
        newPlayer1Tiles.addAll(player1Tiles);
        List<Tile> newPlayer2Tiles = new ArrayList<>();
        newPlayer2Tiles.addAll(player2Tiles);
        BagOfTiles _bagOfTiles = bagOfTiles.deepClone();

        if (player1ToMove) {
            int indexOfPlayedTile = newPlayer1Tiles.indexOf(move.getPlacedTile().getTile());
            
            if (!_bagOfTiles.isEmpty()) {
                Tile tileDrawn;
                if (temporaryTransition) {
                    tileDrawn = _bagOfTiles.peek();
                } else {
                    tileDrawn = _bagOfTiles.draw();
                }
                    newPlayer1Tiles.set(indexOfPlayedTile, tileDrawn);
            } else {
                newPlayer1Tiles.remove(indexOfPlayedTile);
            }
        } else {
            int indexOfPlayedTile = newPlayer2Tiles.indexOf(move.getPlacedTile().getTile());
            
            if (!_bagOfTiles.isEmpty()) {
                Tile tileDrawn;
                if (temporaryTransition) {
                    tileDrawn = _bagOfTiles.peek();
                } else {
                    tileDrawn = _bagOfTiles.draw();
                }
                    newPlayer2Tiles.set(indexOfPlayedTile, tileDrawn);
            } else {
                newPlayer2Tiles.remove(indexOfPlayedTile);
            }
        }
        boolean newFreeMoveHasBeenPlayed = freeMoveHasBeenPlayed;
        if (move.getMoveType().equals(MoveType.FREE)) {
            newFreeMoveHasBeenPlayed = !newFreeMoveHasBeenPlayed;
        }
        GameState newGameState = new GameState(newBoardState, newPlayer1Tiles, newPlayer2Tiles, newFreeMoveHasBeenPlayed, player1ToMove, players, _bagOfTiles);
        if (boardState.getPlacedTiles().size() == 56) {
            gameOver = true;
        }
        return newGameState;
    }

    private Set<Move> findLegalMoves() {
        Set<Move> ret;
        List<Tile> currentPlayerTiles;
        Player currentPlayer;
        if (player1ToMove) {
            currentPlayer = players[0];
            currentPlayerTiles = player1Tiles;
        } else {
            currentPlayer = players[1];
            currentPlayerTiles = player2Tiles;
        }
        ret = boardState.getLegalMoves(currentPlayer, currentPlayerTiles, freeMoveHasBeenPlayed);
        return ret;
    }

    public Set<Move> getLegalMoves() {
        return Collections.unmodifiableSet(legalMoves);
    }

    public boolean isPlayer1ToMove() {
        return player1ToMove;
    }

    public boolean hasFreeMoveBeenPlayed() {
        return freeMoveHasBeenPlayed;
    }

    private void switchPlayer() {
        freeMoveHasBeenPlayed = false;
        player1ToMove = !player1ToMove;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public int[] getScores() {
        int[] scores = new int[2];

        if (gameOver && boardState.getPlacedTiles().size() < 56) {
            return new int[]{0, 0};
        }

        for (int i = 0; i < 2; i++) {
            Queue<Line> playerLines = new PriorityQueue<>();
            TColor playerColor = players[i].getColor();
            Set<PlacedTile> placedTilesAccountedFor = new HashSet<>();
            for (Tile tile : Tile.getAllTilesWithColor(playerColor)) {
                PlacedTile placedTile = boardState.getPlacedTileByTile(tile);
                if (placedTile == null) {
                    continue;
                }
                if (!placedTilesAccountedFor.contains(placedTile)) {
                    Line line = new Line(playerColor, placedTile);
                    placedTilesAccountedFor.add(placedTile);
                    Set<PlacedTile> neighboringPlacedTilesForColor = getNeighboringPlacedTilesForColor(placedTile, playerColor);

                    for (PlacedTile pt : neighboringPlacedTilesForColor) {
                        PlacedTile previousPlacedTileInLine = placedTile;
                        PlacedTile nextPlacedTileInLine = pt;

                        while (true) {
                            if (nextPlacedTileInLine != null) {
                                if (!placedTilesAccountedFor.contains(nextPlacedTileInLine)) {
                                    line.addPlacedTile(nextPlacedTileInLine);
                                    placedTilesAccountedFor.add(nextPlacedTileInLine);
                                } else {
                                    line.setLoop(true);
                                    break;
                                }
                            } else {
                                break;
                            }

                            Set<PlacedTile> nptfc = getNeighboringPlacedTilesForColor(nextPlacedTileInLine, playerColor);
                            nptfc.remove(previousPlacedTileInLine);
                            previousPlacedTileInLine = nextPlacedTileInLine;
                            if (nptfc.isEmpty()) {
                                nextPlacedTileInLine = null;
                            } else {
                                nextPlacedTileInLine = nptfc.iterator().next();
                            }
                        }
                    }

                    playerLines.add(line);
                }
            }

            Line bestLine = playerLines.poll();
            if (bestLine != null) {
                scores[i] = bestLine.getValue();
            } else {
                scores[i] = 0;
            }
        }

        return scores;
    }

    public int getScoreDifference() {
        int[] scores = getScores();
        return scores[0] - scores[1];
    }

    private Set<PlacedTile> getNeighboringPlacedTilesForColor(PlacedTile placedTile, TColor color) {
        Set<PlacedTile> neighboringPlacedTilesForColor = new HashSet<>();
        for (int i = 0; i < 6; i++) {
            if (placedTile.getSequence()[i].equals(color)) {
                PlacedTile neighboringTileWithColor = boardState.getPlacedTileByIntegratedCoordinates(placedTile.getNeighboringTileSpace(TileRotation.getInstance(i * 60)));
                if (neighboringTileWithColor != null) {
                    neighboringPlacedTilesForColor.add(neighboringTileWithColor);
                }
            }
        }
        return neighboringPlacedTilesForColor;
    }

    public double getTPScoreFromScores(int[] scores) {
        int scoreDiff = Math.abs(scores[0] - scores[1]);
        double winnerScore;
        switch (scoreDiff) {
            case 0:
                winnerScore = 10.0;
                break;
            case 1:
                winnerScore = 13.3;
                break;
            case 2:
                winnerScore = 13.9;
                break;
            case 3:
                winnerScore = 14.3;
                break;
            case 4:
                winnerScore = 14.7;
                break;
            case 5:
                winnerScore = 15.0;
                break;
            case 6:
                winnerScore = 15.3;
                break;
            case 7:
                winnerScore = 15.5;
                break;
            case 8:
                winnerScore = 15.8;
                break;
            case 9:
                winnerScore = 16.0;
                break;
            case 10:
                winnerScore = 16.2;
                break;
            case 11:
                winnerScore = 16.4;
                break;
            case 12:
                winnerScore = 16.6;
                break;
            case 13:
                winnerScore = 16.8;
                break;
            case 14:
                winnerScore = 17.0;
                break;
            case 15:
                winnerScore = 17.2;
                break;
            case 16:
                winnerScore = 17.3;
                break;
            case 17:
                winnerScore = 17.5;
                break;
            case 18:
                winnerScore = 17.7;
                break;
            case 19:
                winnerScore = 17.8;
                break;
            case 20:
                winnerScore = 18.0;
                break;
            case 21:
                winnerScore = 18.1;
                break;
            case 22:
                winnerScore = 18.3;
                break;
            case 23:
                winnerScore = 18.4;
                break;
            case 24:
                winnerScore = 18.5;
                break;
            case 25:
                winnerScore = 18.7;
                break;
            case 26:
                winnerScore = 18.8;
                break;
            case 27:
                winnerScore = 18.9;
                break;
            case 28:
                winnerScore = 19.1;
                break;
            case 29:
                winnerScore = 19.2;
                break;
            case 30:
                winnerScore = 19.3;
                break;
            case 31:
                winnerScore = 19.4;
                break;
            case 32:
                winnerScore = 19.5;
                break;
            case 33:
                winnerScore = 19.7;
                break;
            case 34:
                winnerScore = 19.8;
                break;
            case 35:
                winnerScore = 19.9;
                break;
            default:
                winnerScore = 20.0;
                break;
        }
        if (scores[0] > scores[1]) {
            return winnerScore;
        } else {
            return 20.0 - winnerScore;
        }
    }

}
