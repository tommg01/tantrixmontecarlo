
package com.paragon.objects;

import com.paragon.enums.MoveType;

public class Move {

    private final Player player;
    private final PlacedTile placedTile;
    private final MoveType moveType;

    public Move(Player player, PlacedTile placedTile, MoveType moveType) {
        this.player = player;
        this.placedTile = placedTile;
        this.moveType = moveType;
    }

    public Player getPlayer() {
        return player;
    }

    public PlacedTile getPlacedTile() {
        return placedTile;
    }

    public MoveType getMoveType() {
        return moveType;
    }
    
}
