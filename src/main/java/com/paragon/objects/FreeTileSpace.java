package com.paragon.objects;

import com.paragon.enums.TColor;
import com.paragon.enums.TileRotation;
import java.util.HashSet;
import java.util.Set;

public class FreeTileSpace extends TileSpace {

    private int colors = 0;
    private boolean forced;
    private boolean forbidden;
    
    public FreeTileSpace(int integratedCoordinates, TColor[] sequence, boolean forced, boolean forbidden) {
        this.coordinates = splitCoordinates(integratedCoordinates);
        this.sequence = sequence;
        this.forced = forced;
        this.forbidden = forbidden;
    }

    public FreeTileSpace(int[] coordinates, TColor[] sequence, boolean forced, boolean forbidden) {
        this.coordinates = coordinates;
        this.sequence = sequence;
        this.forced = forced;
        this.forbidden = forbidden;
    }
    
    public FreeTileSpace(int integratedCoordinates) {
        this.coordinates = splitCoordinates(integratedCoordinates);
        this.sequence = new TColor[] {null, null, null, null, null, null};
        this.forced = false;
        this.forbidden = false;
    }

    public boolean isForced() {
        return forced;
    }

    public boolean isForbidden() {
        return forbidden;
    }

    public boolean addColorAndGetWhetherTileSpaceBecameForced(TileRotation rotation, TColor color) {
        boolean becameForcedSpace = false;
        int index = rotation.getRotation() / 60;
        if (sequence[index] != null) {
            System.out.println("Cannot add color to tile space because it is already filled.");
            System.exit(1);
        }
        sequence[index] = color;
        colors++;
        if (colors > 2) {
            if (!forced) {
                becameForcedSpace = true;
            }
            forced = true;
        }
        return becameForcedSpace;
    }

    public Set<TileRotation> getFittingTileRotations(Tile tile) {
        Set<TileRotation> fittingTileRotations = new HashSet<>();
        outerLoop:
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (this.sequence[j] != null && this.sequence[j] != tile.getSequence()[(j - i + 6) % 6]) {
                    continue outerLoop;
                }
            }
            TileRotation fittingTileRotation = TileRotation.getInstance(i * 60);
            fittingTileRotations.add(fittingTileRotation);
        }
        // Only one rotation on empty board
        if (fittingTileRotations.size() == 6) {
            fittingTileRotations.clear();
            fittingTileRotations.add(TileRotation.ZERO);
        }
        
        return fittingTileRotations;
    }

    public void setForced(boolean forced) {
        this.forced = forced;
    }

    public void setForbidden(boolean forbidden) {
        this.forbidden = forbidden;
    }

}
