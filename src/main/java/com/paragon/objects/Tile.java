package com.paragon.objects;

import com.paragon.enums.TColor;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Tile {

    private static final TColor B = TColor.BLUE;
    private static final TColor G = TColor.GREEN;
    private static final TColor R = TColor.RED;
    private static final TColor Y = TColor.YELLOW;
    
    private static final Map<Integer, Tile> TILES = new HashMap<>();
    private static final Map<TColor, Set<Tile>> TILES_BY_COLOR = new HashMap<>();
    
    static {
        TILES.put(1, new Tile(1, 37, new TColor[] {R, Y, Y, B, R, B}));
        TILES.put(2, new Tile(2, 33, new TColor[] {B, Y, Y, B, R, R}));
        TILES.put(3, new Tile(3, 29, new TColor[] {Y, Y, R, R, B, B}));
        TILES.put(4, new Tile(4, 36, new TColor[] {B, Y, R, B, R, Y}));
        TILES.put(5, new Tile(5, 32, new TColor[] {R, Y, Y, R, B, B}));
        TILES.put(6, new Tile(6, 34, new TColor[] {Y, R, B, Y, B, R}));
        TILES.put(7, new Tile(7, 42, new TColor[] {R, B, B, Y, R, Y}));
        TILES.put(8, new Tile(8, 41, new TColor[] {Y, B, B, R, Y, R}));
        TILES.put(9, new Tile(9, 35, new TColor[] {R, Y, B, R, B, Y}));
        TILES.put(10, new Tile(10, 38, new TColor[] {B, Y, Y, R, B, R}));
        TILES.put(11, new Tile(11, 39, new TColor[] {Y, R, R, B, Y, B}));
        TILES.put(12, new Tile(12, 40, new TColor[] {B, R, R, Y, B, Y}));
        TILES.put(13, new Tile(13, 31, new TColor[] {Y, R, R, Y, B, B}));
        TILES.put(14, new Tile(14, 30, new TColor[] {R, R, Y, Y, B, B}));
        TILES.put(15, new Tile(15, 5, new TColor[] {R, Y, Y, R, G, G}));
        TILES.put(16, new Tile(16, 3, new TColor[] {Y, G, G, Y, R, R}));
        TILES.put(17, new Tile(17, 10, new TColor[] {R, Y, Y, G, R, G}));
        TILES.put(18, new Tile(18, 9, new TColor[] {G, Y, Y, R, G, R}));
        TILES.put(19, new Tile(19, 13, new TColor[] {Y, R, R, G, Y, G}));
        TILES.put(20, new Tile(20, 14, new TColor[] {G, R, R, Y, G, Y}));
        TILES.put(21, new Tile(21, 1, new TColor[] {Y, Y, G, G, R, R}));
        TILES.put(22, new Tile(22, 4, new TColor[] {G, Y, Y, G, R, R}));
        TILES.put(23, new Tile(23, 2, new TColor[] {Y, Y, R, R, G, G}));
        TILES.put(24, new Tile(24, 47, new TColor[] {B, G, G, B, R, R}));
        TILES.put(25, new Tile(25, 44, new TColor[] {G, G, B, B, R, R}));
        TILES.put(26, new Tile(26, 45, new TColor[] {G, R, R, G, B, B}));
        TILES.put(27, new Tile(27, 53, new TColor[] {G, R, R, B, G, B}));
        TILES.put(28, new Tile(28, 43, new TColor[] {G, G, R, R, B, B}));
        TILES.put(29, new Tile(29, 54, new TColor[] {B, R, R, G, B, G}));
        TILES.put(30, new Tile(30, 46, new TColor[] {R, B, B, R, G, G}));
        TILES.put(31, new Tile(31, 11, new TColor[] {Y, G, G, R, Y, R}));
        TILES.put(32, new Tile(32, 7, new TColor[] {G, Y, R, G, R, Y}));
        TILES.put(33, new Tile(33, 12, new TColor[] {R, G, G, Y, R, Y}));
        TILES.put(34, new Tile(34, 8, new TColor[] {R, Y, G, R, G, Y}));
        TILES.put(35, new Tile(35, 6, new TColor[] {Y, G, R, Y, R, G}));
        TILES.put(36, new Tile(36, 52, new TColor[] {B, G, G, R, B, R}));
        TILES.put(37, new Tile(37, 56, new TColor[] {R, B, B, G, R, G}));
        TILES.put(38, new Tile(38, 55, new TColor[] {G, B, B, R, G, R}));
        TILES.put(39, new Tile(39, 48, new TColor[] {G, R, B, G, B, R}));
        TILES.put(40, new Tile(40, 50, new TColor[] {B, G, R, B, R, G}));
        TILES.put(41, new Tile(41, 51, new TColor[] {R, G, G, B, R, B}));
        TILES.put(42, new Tile(42, 49, new TColor[] {R, G, B, R, B, G}));
        TILES.put(43, new Tile(43, 15, new TColor[] {Y, Y, G, G, B, B}));
        TILES.put(44, new Tile(44, 20, new TColor[] {Y, G, B, Y, B, G}));
        TILES.put(45, new Tile(45, 16, new TColor[] {Y, Y, B, B, G, G}));
        TILES.put(46, new Tile(46, 25, new TColor[] {Y, G, G, B, Y, B}));
        TILES.put(47, new Tile(47, 17, new TColor[] {Y, G, G, Y, B, B}));
        TILES.put(48, new Tile(48, 19, new TColor[] {B, Y, Y, B, G, G}));
        TILES.put(49, new Tile(49, 18, new TColor[] {G, Y, Y, G, B, B}));
        TILES.put(50, new Tile(50, 22, new TColor[] {B, Y, G, B, G, Y}));
        TILES.put(51, new Tile(51, 21, new TColor[] {G, Y, B, G, B, Y}));
        TILES.put(52, new Tile(52, 26, new TColor[] {B, G, G, Y, B, Y}));
        TILES.put(53, new Tile(53, 24, new TColor[] {B, Y, Y, G, B, G}));
        TILES.put(54, new Tile(54, 23, new TColor[] {G, Y, Y, B, G, B}));
        TILES.put(55, new Tile(55, 27, new TColor[] {Y, B, B, G, Y, G}));
        TILES.put(56, new Tile(56, 28, new TColor[] {G, B, B, Y, G, Y}));
        
        TILES_BY_COLOR.put(B, new HashSet<>());
        TILES_BY_COLOR.put(G, new HashSet<>());
        TILES_BY_COLOR.put(R, new HashSet<>());
        TILES_BY_COLOR.put(Y, new HashSet<>());
        
        for (Tile tile: TILES.values()) {
            for (TColor color: tile.getSequence()) {
                TILES_BY_COLOR.get(color).add(tile);
            }
        }
    }


    private final int id;
    private final int altId;
    private final TColor[] sequence;

    private Tile(int id, int altId, TColor[] sequence) {
        this.id = id;
        this.altId = altId;
        this.sequence = sequence;
    }

    public static Tile getInstance(int id) {
        return TILES.get(id);
    }
    
    public static Collection<Tile> getAllTiles() {
        return Collections.unmodifiableCollection(TILES.values());
    }
    
    public static Set<Tile> getAllTilesWithColor(TColor color) {
        return Collections.unmodifiableSet(TILES_BY_COLOR.get(color));
    }

    public int getId() {
        return id;
    }

    public TColor[] getSequence() {
        return sequence;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
    
}
