package com.paragon.objects;

import com.paragon.enums.TColor;
import java.util.HashSet;
import java.util.Set;

public class Line implements Comparable<Line> {
    
    public final TColor color;
    public final Set<PlacedTile> placedTiles;
    public boolean loop;
    
    public Line (TColor color, PlacedTile placedTile) {
        this.color = color;
        this.placedTiles = new HashSet<>();
        addPlacedTile(placedTile);
    }

    public void addPlacedTile(PlacedTile placedTile) {
        this.placedTiles.add(placedTile);
    }
    
    public int getValue() {
        if (loop) {
            return 2 * placedTiles.size();
        }
        return placedTiles.size();
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    @Override
    public int compareTo(Line o) {
        return o.getValue() - this.getValue();
    }
    
}
