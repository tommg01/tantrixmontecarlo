package com.paragon.objects;

import com.paragon.enums.TColor;

public class Player {
    
    private final String name;
    private final TColor color;
    
    public Player(String name, TColor color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public TColor getColor() {
        return color;
    }
    
}
