package com.paragon.objects;

import com.paragon.objects.agents.Agent;
import com.paragon.enums.GameStatus;
import com.paragon.objects.frames.Board;
import com.paragon.tantrixmontecarlo.TantrixMonteCarlo;
import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.List;

public class Game implements Runnable {

    private final UID id;
    private final Agent[] agents;
    private final Player[] players;
    private final int maxTimeInSeconds;
    private final List<GameState> gameStates;
    private final boolean displayBoard;
    private GameState currentGameState;
    private GameStatus gameStatus;
    private long gameStart;
    private long gameEnd;
    private Board board = null;

    public Game(Agent[] agents, Player[] player, int maxTimeInSeconds, boolean displayBoard) {
        this.id = new UID();
        this.agents = agents;
        this.players = player;
        this.maxTimeInSeconds = maxTimeInSeconds;
        this.displayBoard = displayBoard;
        this.gameStates = new ArrayList<>();
        this.gameStatus = GameStatus.INITIALIZED;
    }

    @Override
    public void run() {
        gameStatus = GameStatus.RUNNING;
        gameStart = System.currentTimeMillis();

        // Set up initial game state
        currentGameState = new GameState(players);
        if (displayBoard) {
            board = new Board(currentGameState, players);
            board.display();
        }
        do {
            gameStates.add(currentGameState);
            Agent currentAgent;
            if (currentGameState.isPlayer1ToMove()) {
                currentAgent = agents[0];
            } else {
                currentAgent = agents[1];
            }
            List<Move> legalMoves = new ArrayList<>();
            legalMoves.addAll(currentGameState.getLegalMoves());
            if (legalMoves.isEmpty()) {
                break;
            }

            Move move = currentAgent.getMove(currentGameState, maxTimeInSeconds);

            GameState newGameState = currentGameState.transition(move, false, "game");
            if (displayBoard) {
                board.setGameState(newGameState);
            }
            currentGameState = newGameState;
        } while (!currentGameState.isGameOver());
        gameStates.add(currentGameState);

        gameStatus = GameStatus.FINISHED;
        gameEnd = System.currentTimeMillis();
        TantrixMonteCarlo.addFinishedGame(this);
    }

    public GameState getCurrentGameState() {
        return gameStates.get(gameStates.size() - 1);
    }

    public Player[] getPlayers() {
        return players;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public UID getId() {
        return id;
    }

    public long getGameStart() {
        return gameStart;
    }

    public long getGameEnd() {
        return gameEnd;
    }

    public List<GameState> getGameStates() {
        return gameStates;
    }

    public double getCurrentTPs() {
        return getCurrentGameState().getTPScoreFromScores(getCurrentGameState().getScores());
    }

}
