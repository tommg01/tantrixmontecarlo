package com.paragon.objects.agents;

import com.paragon.objects.GameState;
import com.paragon.objects.Move;

/**
 * Selects a random move out of the moves where placing that one tile increase
 * the immediate game score difference by the most.
 */
public class LineBuilder1Agent extends Agent {

    public LineBuilder1Agent(String name, boolean isP1, int gameNo) {
        super(name, isP1, gameNo);
    }

    @Override
    protected void setEvaluator() {
        evaluator = () -> {
            for (Move move : evaluations.keySet()) {
                GameState tempGameState = gameState.transition(move, true, "lb");
                int tempScoreDiff = tempGameState.getScoreDifference();
                if (!isP1) {
                    tempScoreDiff *= -1;
                }
                evaluations.put(move, (double) tempScoreDiff);
            }
            return null;
        };
    }

}
