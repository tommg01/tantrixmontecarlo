package com.paragon.objects.agents;

import com.paragon.objects.Move;

/**
 * Selects its moves at random.
 */
public class RandomAgent extends Agent {

    public RandomAgent(String name, boolean isP1, int gameNo) {
        super(name, isP1, gameNo);
    }

    @Override
    protected void setEvaluator() {
        evaluator = () -> {
            for (Move move : evaluations.keySet()) {
                evaluations.put(move, 0.0);
            }
            return null;
        };
    }

}
