package com.paragon.objects.agents;

import com.paragon.objects.GameState;
import com.paragon.objects.Move;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 */
public class MonteCarlo1Agent extends Agent {

    private Map<Move, Integer> simulationCounts = new HashMap<>();
    private Map<Move, Double> simulationValueSums = new HashMap<>();

    public MonteCarlo1Agent(String name, boolean isP1, int gameNo) {
        super(name, isP1, gameNo);
    }

    @Override
    protected void setEvaluator() {
        evaluator = () -> {
            long startTime = System.currentTimeMillis();
//            try {
            if (evaluations.size() < 2) {
                for (Move move : evaluations.keySet()) {
                    evaluations.put(move, 0.0);
                }
                return null;
            }

            simulationCounts.clear();
            simulationValueSums.clear();
            for (Move move : evaluations.keySet()) {
                simulationCounts.put(move, 0);
                simulationValueSums.put(move, 0.0);
            }
            GameState gameStateClone = gameState.deepClone();
            for (int i = 0; i < 50000; i++) {
                if (System.currentTimeMillis() - startTime > maxTimeInSeconds * 1000)  {
                    break;
                }
//                System.out.println(gameNo + "\t" + gameState.getBoardState().getPlacedTiles().size() + "\t" + i);
                for (Move move : gameStateClone.getLegalMoves()) {
                    GameState gameStateClone2 = gameStateClone.deepClone();
                    if (!gameStateClone2.getLegalMoves().contains(move)) {
                        System.out.println("LOL " + gameStateClone.getLegalMoves().contains(move));
                        System.out.println(gameStateClone.getLegalMoves().size());
                        System.exit(1);
                    }
                    double score = runSimulation(gameStateClone2, move);
                    if (!isP1) {
                        score = 20.0 - score;
                    }

                    simulationCounts.put(move, simulationCounts.get(move) + 1);
                    simulationValueSums.put(move, simulationValueSums.get(move) + score);
                    evaluations.put(move, simulationValueSums.get(move) / simulationCounts.get(move));
                }
            }
//            } catch (ArrayIndexOutOfBoundsException e) {
//                System.out.println();
//            }
            return null;
        };
    }

    private double runSimulation(GameState _gameState, Move move) {
        GameState currentGameState = _gameState.transition(move, false, "mcsim1");
        while (!currentGameState.isGameOver()) {
            Set<Move> legalMoves = new HashSet<>();
            legalMoves.addAll(currentGameState.getLegalMoves());
            if (legalMoves.isEmpty()) {
                break;
            }

            Move nextMove = getRandomMove(legalMoves);

            GameState newGameState = currentGameState.transition(nextMove, false, "mcsim2");
            currentGameState = newGameState;
        }
        return currentGameState.getTPScoreFromScores(currentGameState.getScores());
    }

}
