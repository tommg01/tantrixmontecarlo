package com.paragon.objects.agents;

import com.paragon.enums.MoveType;
import com.paragon.objects.GameState;
import com.paragon.objects.Move;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class Agent {

    private final String name;
    protected final boolean isP1;
    protected Callable<Void> evaluator;
    protected final Map<Move, Double> evaluations;
    protected GameState gameState;
    protected int maxTimeInSeconds = 0;
    int gameNo;

    protected Agent(String name, boolean isP1, int gameNo) {
        this.name = name;
        this.isP1 = isP1;
        this.evaluations = new HashMap<>();
        gameState = null;
        setEvaluator();
        this.gameNo = gameNo;
    }

    public String getName() {
        return name;
    }

    public boolean isIsP1() {
        return isP1;
    }

    public Move getMove(GameState gameState, int maxTimeInSeconds) {
        this.gameState = gameState;
        this.maxTimeInSeconds = maxTimeInSeconds;
        evaluations.clear();
        for (Move legalMove : gameState.getLegalMoves()) {
            evaluations.put(legalMove, null);
        }
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<Void> future = executorService.submit(evaluator);
        try {
            future.get(maxTimeInSeconds, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            System.out.println(e);
        } catch (TimeoutException f) {
            
        }
        future.cancel(true);
        executorService.shutdownNow();
        
        Set<Move> bestMoves = new HashSet<>();
        Double bestEvaluation = null;
        List<Entry<Move, Double>> moveRanking = new ArrayList<>();
        for (Entry<Move, Double> evaluation : evaluations.entrySet()) {
            if (!gameState.getLegalMoves().contains(evaluation.getKey())) {
//                System.out.println("Non-legal move detected! ");
                continue;
            }
            if (evaluation.getValue() != null) {
                moveRanking.add(evaluation);
            }
            if (bestEvaluation == null || (evaluation.getValue() != null && evaluation.getValue() > bestEvaluation)) {
                bestMoves.clear();
                bestEvaluation = evaluation.getValue();
                bestMoves.add(evaluation.getKey());
            } else if (evaluation.getValue() != null && evaluation.getValue().doubleValue() == bestEvaluation) {
                bestMoves.add(evaluation.getKey());
            }
        }
        moveRanking.sort(Entry.comparingByValue());
//        System.out.println("--------");
//        for (Entry<Move, Double> evaluation : moveRanking) {
//            System.out.println(evaluation.getKey().getPlacedTile().getTile().getId() + "\t"
//                    + Arrays.toString(evaluation.getKey().getPlacedTile().getCoordinates()) + "\t"
//                    + evaluation.getKey().getPlacedTile().getRotation() + "\t" + String.format("%.2f", evaluation.getValue()));
//        }
        return getRandomMove(bestMoves);
    }

    protected Move getRandomMove(Set<Move> bestMoves) {
        int index = new Random().nextInt(bestMoves.size());
        Iterator<Move> iter = bestMoves.iterator();
        for (int i = 0; i < index; i++) {
            iter.next();
        }
        return iter.next();
    }

    protected abstract void setEvaluator();

}
