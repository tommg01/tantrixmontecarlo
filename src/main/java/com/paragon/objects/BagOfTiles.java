package com.paragon.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class BagOfTiles {

    private final List<Tile> bag;

    public BagOfTiles() {
        bag = new ArrayList<>();
    }

    public void fill() {
        bag.addAll(Tile.getAllTiles());
        Collections.shuffle(bag);
    }

    public BagOfTiles deepClone() {
        BagOfTiles _bagOfTiles = new BagOfTiles();
        _bagOfTiles.addAll(bag);
        return _bagOfTiles;
    }
//    
//    public Set<Tile> getBag() {
//        Set<Tile> ret = new HashSet<>();
//        ret.addAll(bag);
//        return ret;
//    }

    public void remove(Tile tile) {
        bag.remove(tile);
    }

    public void removeAll(Collection<Tile> tiles) {
        bag.removeAll(tiles);
    }
    
    public void addAll(Collection<Tile> tiles) {
        bag.addAll(tiles);
    }

    public boolean isEmpty() {
        return bag.isEmpty();
    }

    public Tile peek() {
        if (isEmpty()) {
            return null;
        }
        Tile tile = bag.get(0);
        return tile;
    }

    public Tile draw() {
        Tile tile = peek();
        bag.remove(tile);
        return tile;
    }
    
    public int size() {
        return bag.size();
    }
    
    public List<Tile> getBag() {
        return Collections.unmodifiableList(bag);
    }

}
