package com.paragon.objects;

import com.paragon.enums.TColor;
import com.paragon.enums.MoveType;
import com.paragon.enums.TileRotation;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import static java.util.stream.Collectors.toSet;

public class BoardState {

    private final Map<Integer, PlacedTile> placedTiles;
    private final Map<Tile, PlacedTile> placedTilesByTile;

    public BoardState() {
        this.placedTiles = new HashMap<>();
        this.placedTilesByTile = new HashMap<>();
    }
    
    public BoardState deepClone() {
        BoardState _boardState = new BoardState();
        _boardState.putAllPlacedTiles(placedTiles);
        return _boardState;
    }

    public BoardState transition(Move move) {
        BoardState newBoardState = new BoardState();
        newBoardState.putAllPlacedTiles(placedTiles);
        newBoardState.putPlacedTile(move.getPlacedTile().getIntegratedCoordinates(), move.getPlacedTile());
        return newBoardState;
    }

    public void putAllPlacedTiles(Map<Integer, PlacedTile> _placedTiles) {
        this.placedTiles.putAll(_placedTiles);
        for (PlacedTile placedTile: _placedTiles.values()) {
            this.placedTilesByTile.put(placedTile.getTile(), placedTile);
        }
    }

    public void putPlacedTile(int integratedCoordinates, PlacedTile placedTile) {
        this.placedTiles.put(integratedCoordinates, placedTile);
        this.placedTilesByTile.put(placedTile.getTile(), placedTile);
    }

    public Set<Move> getLegalMoves(Player player, List<Tile> currentPlayerTiles, boolean freeMoveHasBeenPlayed) {
        Set<Move> legalMoves = new HashSet<>();
        Map<Integer, FreeTileSpace> legalSpaces = new HashMap<>();
        if (placedTiles.isEmpty()) {
            FreeTileSpace freeTileSpace = new FreeTileSpace(0);
            legalSpaces.put(0, freeTileSpace);
            return getLegalMovesForLegalSpaces(player, legalSpaces.values(), currentPlayerTiles, MoveType.FREE);
        } else {
            // Get all adjoining spaces
            legalSpaces.putAll(getAllAdjoiningSpaces());

            // Get all forced spaces
            Set<FreeTileSpace> forcedSpaces = legalSpaces.values().stream().filter(l -> l.isForced()).collect(toSet());

            // Check if there are any legal forced moves
            Set<Move> legalForcedMoves = getLegalMovesForLegalSpaces(player, forcedSpaces, currentPlayerTiles, freeMoveHasBeenPlayed ? MoveType.FORCED_AFTER_FREE : MoveType.FORCED_BEFORE_FREE);
            if (!legalForcedMoves.isEmpty()) {
                return legalForcedMoves;
            } else if (!freeMoveHasBeenPlayed) {
                // Find forbidden spaces and set their 'forbidden' attribute true
                Set<Integer> forbiddenSpaces = findForbiddenSpaces(forcedSpaces, legalSpaces);

                // Remove forbidden and forced spaces from collection of legal spaces
                for (int forbiddenSpace : forbiddenSpaces) {
                    legalSpaces.remove(forbiddenSpace);
                }
                for (FreeTileSpace forcedSpace : forcedSpaces) {
                    legalSpaces.remove(forcedSpace.getIntegratedCoordinates());
                }

                // Get legal moves for legal spaces
                legalMoves.addAll(getLegalMovesForLegalSpaces(player, legalSpaces.values(), currentPlayerTiles, MoveType.FREE));
            }
        }
        return legalMoves;
    }

    public Map<Integer, FreeTileSpace> getAllAdjoiningSpaces() {
        Map<Integer, FreeTileSpace> allAdjoiningSpaces = new HashMap<>();
        for (PlacedTile placedTile : placedTiles.values()) {
            Map<Integer, TColor[]> neighboringTileSpaces = placedTile.getNeighboringTileSpaces();
            for (Entry<Integer, TColor[]> neighbor : neighboringTileSpaces.entrySet()) {
                if (!placedTiles.containsKey(neighbor.getKey())) {
                    if (!allAdjoiningSpaces.containsKey(neighbor.getKey())) {
                        FreeTileSpace freeTileSpace = new FreeTileSpace(neighbor.getKey());
                        allAdjoiningSpaces.put(neighbor.getKey(), freeTileSpace);
                    }
                    TileRotation tileRotation = null;
                    TColor color = null;
                    for (int i = 0; i < 6; i++) {
                        if (neighbor.getValue()[i] != null) {
                            tileRotation = TileRotation.getInstance(i * 60);
                            color = neighbor.getValue()[i];
                            break;
                        }
                    }
                    allAdjoiningSpaces.get(neighbor.getKey()).addColorAndGetWhetherTileSpaceBecameForced(tileRotation, color);
                }
            }
        }
        return allAdjoiningSpaces;
    }

    public Set<Integer> findForbiddenSpaces(Set<FreeTileSpace> forcedSpaces, Map<Integer, FreeTileSpace> legalSpaces) {
        Set<Integer> forbiddenSpaces = new HashSet<>();
        if (placedTiles.size() < 44) {
            for (FreeTileSpace forcedSpace : forcedSpaces) {
                Map<TileRotation, Integer> forcedSpaceNeighbors = forcedSpace.getNeighboringTileSpacesWithRotation();

                for (Entry<TileRotation, Integer> forcedSpaceNeighbor : forcedSpaceNeighbors.entrySet()) {
                    int nextForbidden = forcedSpaceNeighbor.getValue();
                    while (true) {
                        if (legalSpaces.containsKey(nextForbidden)) {
                            legalSpaces.get(nextForbidden).setForbidden(true);
                            forbiddenSpaces.add(nextForbidden);
                        } else {
                            break;
                        }
                        nextForbidden = legalSpaces.get(nextForbidden).getNeighboringTileSpacesWithRotation().get(forcedSpaceNeighbor.getKey());
                    }
                }
            }
        }
        return forbiddenSpaces;
    }

    private Set<Move> getLegalMovesForLegalSpaces(Player player, Collection<FreeTileSpace> legalSpaces, List<Tile> currentPlayerTiles, MoveType moveType) {
        Set<Move> legalMoves = new HashSet<>();

        for (FreeTileSpace legalSpace : legalSpaces) {
            for (Tile tile : currentPlayerTiles) {
                Set<TileRotation> fittingTileRotations = legalSpace.getFittingTileRotations(tile);
                if (!fittingTileRotations.isEmpty()) {
                    for (TileRotation fittingTileRotation : fittingTileRotations) {
                        PlacedTile placedTile = new PlacedTile(tile, legalSpace.getCoordinates(), fittingTileRotation);
                        Move move = new Move(player, placedTile, moveType);
                        if (!(moveCreatesInvalidSpace(move) && placedTiles.size() < 44)) {
                            legalMoves.add(move);
                        }
                    }
                }
            }
        }

        return legalMoves;
    }

    public Map<Integer, PlacedTile> getPlacedTiles() {
        return Collections.unmodifiableMap(placedTiles);
    }
    
    public PlacedTile getPlacedTileByTile(Tile tile) {
        return placedTilesByTile.get(tile);
    }
    
    public PlacedTile getPlacedTileByIntegratedCoordinates(int integratedCoordinates) {
        return placedTiles.get(integratedCoordinates);
    }

    public boolean moveCreatesInvalidSpace(Move move) {
        Map<TileRotation, Integer> neighboringTileSpaces = move.getPlacedTile().getNeighboringTileSpacesWithRotation();
        for (Entry<TileRotation, Integer> neighboringTileSpace: neighboringTileSpaces.entrySet()) {
            if (!placedTiles.containsKey(neighboringTileSpace.getValue())) {
                TColor color = move.getPlacedTile().getSequence()[neighboringTileSpace.getKey().getRotation() / 60];
                int countOfColor = 0;
                FreeTileSpace fts = new FreeTileSpace(neighboringTileSpace.getValue());
                for (Entry<TileRotation, Integer> nts: fts.getNeighboringTileSpacesWithRotation().entrySet()) {
                    if (placedTiles.containsKey(nts.getValue()) 
                            && placedTiles.get(nts.getValue()).getSequence()[nts.getKey().getOpposite().getRotation() / 60].equals(color)) {
                        countOfColor++;
                    }
                }
                if (countOfColor > 1) {
                    return true;
                }
            }
        }
        return false;
    }

}
